update DCT_MAP_ORG_TO_HIER_PROD set dwh_division_code='CZ-MBCZ_TRUCK' where country_cd='CZ' and division_new in 
('TRUCK','Unimog','Industrialengine','Mitsubishi');

update DCT_MAP_ORG_TO_HIER_PROD set dwh_division_code='CZ-MBCZ_PC' where country_cd='CZ' and division_new in 
('PC','Smart');
commit;
update DCT_MAP_ORG_TO_HIER_PROD set dwh_division_code='CZ-MBCZ_VAN' where country_cd='CZ' and division_new in 
('VAN');
commit;




update DCT_MAP_ORG_TO_P2D set DWH_DIVISION_CODE='CZ-MBCZ_TRUCK' where division_new in ('Fuso','TRUCK','Unimog') and country_cd='CZ';
update DCT_MAP_ORG_TO_P2D set DWH_DIVISION_CODE='CZ-MBCZ_PC' where division_new in ('Smart','PC') and country_cd='CZ';
update DCT_MAP_ORG_TO_P2D set DWH_DIVISION_CODE='CZ-MBCZ_VAN' where division_new in ('VAN')  and country_cd='CZ';
commit;

insert into DCT_MAP_ORG_TO_P2D (country_cd,dc,division_new,dwh_division_code)
values ('CZ','#C','PC','CZ-MBCZ_PC');
insert into DCT_MAP_ORG_TO_P2D (country_cd,dc,division_new,dwh_division_code)
values ('CZ','VC','PC','CZ-MBCZ_PC');
insert into DCT_MAP_ORG_TO_P2D (country_cd,dc,division_new,dwh_division_code)
values ('CZ','IC','PC','CZ-MBCZ_PC');

insert into DCT_MAP_ORG_TO_P2D (country_cd,dc,division_new,dwh_division_code)
values ('CZ','#V','VAN','CZ-MBCZ_VAN');
insert into DCT_MAP_ORG_TO_P2D (country_cd,dc,division_new,dwh_division_code)
values ('CZ','CV','VAN','CZ-MBCZ_VAN');
commit;



update  dim_dwh_org set dwh_division_code='CZ-MBCZ_VAN' where dwh_division_code='CZ-MBCZ';
insert into dim_dwh_org(country_cd,company,dwh_division_code)values
('CZ',	'MBCZ','CZ-MBCZ_TRUCK');
insert into dim_dwh_org(country_cd,company,dwh_division_code)values
('CZ',	'MBCZ','CZ-MBCZ_PC');
commit;