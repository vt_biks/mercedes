--Zmiana division_new dla fuso ponieważ nie mapowało sie z dim_hier_prod
update DCT_MAP_ORG_TO_HIER_PROD set division_new='Mitsubishi' where division_new='FUSO'


update DCT_MAP_ORG_TO_HIER_PROD set  dwh_division_code ='CZ-MBCZ-PC' where dwh_division_code='CZ-MBCZ_PC';
update DCT_MAP_ORG_TO_HIER_PROD set  dwh_division_code ='CZ-MBCZ-TRUCK' where dwh_division_code='CZ-MBCZ_TRUCK';
update DCT_MAP_ORG_TO_HIER_PROD set  dwh_division_code ='CZ-MBCZ-VAN' where dwh_division_code='CZ-MBCZ_VAN';
commit;
update DCT_MAP_ORG_TO_P2D set  dwh_division_code ='CZ-MBCZ-PC' where dwh_division_code='CZ-MBCZ_PC';
update DCT_MAP_ORG_TO_P2D set  dwh_division_code ='CZ-MBCZ-TRUCK' where dwh_division_code='CZ-MBCZ_TRUCK';
update DCT_MAP_ORG_TO_P2D set  dwh_division_code ='CZ-MBCZ-VAN' where dwh_division_code='CZ-MBCZ_VAN';